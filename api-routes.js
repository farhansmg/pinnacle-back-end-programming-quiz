// Initialize express router
let router = require('express').Router();

// Set default API response
router.get('/', function (req, res) {
    res.json({
       status: 'API Its Working',
       message: 'Welcome to BookHub crafted with love!',
    });
});

// Import book controller
var bookController = require('./bookController');

// Book routes
router.route('/books')
    .get(bookController.index)
    .post(bookController.new);

router.route('/books/:id')
    .get(bookController.view)
    .put(bookController.update)
    .delete(bookController.delete);

// Export API routes
module.exports = router;